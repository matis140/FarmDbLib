use diesel;
use diesel::prelude::*;
use ConPool;

use plant_models::schema::plant::plants;

/*
CREATE TABLE IF NOT EXISTS plant.plants (
    id SERIAL PRIMARY KEY,
    family TEXT NOT NULL,
    genus TEXT NOT NULL,
    species TEXT NOT NULL,
    variety TEXT
);
*/

#[derive(Insertable)]
#[table_name="plants"]
pub struct NewPlant<'a> {
    pub family: &'a str,
    pub genus: &'a str,
    pub species: &'a str,
    pub variety: &'a str,
}

#[allow(dead_code)]
#[derive(Queryable, Identifiable, PartialEq)]
pub struct Plant {
    pub id: i32,
    pub family: String,
    pub genus: String,
    pub species: String,
    pub variety: String,
}

pub fn create_plant<'a>(p_family: &'a str, p_genus: &'a str, p_species: &'a str, p_variety: &'a str) -> Plant {
    let new_plant = NewPlant {
        family: p_family,
        genus: p_genus,
        species: p_species,
        variety: p_variety,
    };
    let pool = ConPool::get_connection();
    diesel::insert(&new_plant)
        .into(plants::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new plant")
}


pub fn get_plant(p_id: i32) -> Option<Plant> {
    use self::plants::dsl::*;
    let pool = ConPool::get_connection();
    let dplant = match plants.filter(id.eq(p_id)).get_result::<Plant>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dplant
}

pub fn get_plants() -> Option<Vec<Plant>> {
    use self::plants::dsl::*;
    let pool = ConPool::get_connection();
    let dplants = match plants.load::<Plant>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dplants
}

pub fn update_plant(p_plant: Plant) -> bool {
    use self::plants::dsl::*;
    let pool = ConPool::get_connection();
    let p_db_current = plants.filter(id.eq(p_plant.id)).get_result::<Plant>(&*pool.get().unwrap()).unwrap();
    let mut update: bool = false;

    if p_plant.family != p_db_current.family ||
        p_plant.genus != p_db_current.genus ||
        p_plant.species != p_db_current.species ||
        p_plant.variety != p_db_current.variety {
        update = true;
    }

    let mut did_update: bool = false;
    if update {
        did_update = update_dif_columns(&p_db_current, &p_plant);
    }
    if update && !did_update {
        update = false;
    }

    update
}

#[allow(unused_must_use)]
fn update_dif_columns(p_orig: &Plant, p_targ: &Plant) -> bool {
    let mut complete: bool = false;
    use self::plants::dsl::*;
    let pool = ConPool::get_connection();
    if !p_orig.family.eq(&p_targ.family) {
         diesel::update(plants.filter(id.eq(p_targ.id)))
        .set(family.eq(&p_targ.family))
        .execute(&*pool.get().unwrap());
        complete = true;
    }
    if !p_orig.genus.eq(&p_targ.genus) {
        diesel::update(plants.filter(id.eq(p_targ.id)))
        .set(genus.eq(&p_targ.genus))
        .execute(&*pool.get().unwrap());
        complete = true;
    }
    if !p_orig.species.eq(&p_targ.species) {
        diesel::update(plants.filter(id.eq(p_targ.id)))
        .set(species.eq(&p_targ.species))
        .execute(&*pool.get().unwrap());
        complete = true;
    }
    if !p_orig.variety.eq(&p_targ.species) {
        diesel::update(plants.filter(id.eq(p_targ.id)))
        .set(variety.eq(&p_targ.variety))
        .execute(&*pool.get().unwrap());
        complete = true;
    }

    complete
}
