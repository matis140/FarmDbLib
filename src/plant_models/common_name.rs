use diesel;
use diesel::prelude::*;
use ConPool;
use plant_models::plant::Plant;
use plant_models::schema::plant::{common_names};

#[allow(dead_code)]
#[derive(Queryable, Identifiable, PartialEq, Associations)]
#[belongs_to(Plant)]
pub struct CommonName {
    pub id: i32,
    pub plant_id: i32,
    pub name: String
}

#[derive(Insertable)]
#[table_name="common_names"]
pub struct NewCommonName<'a> {
    plant_id: i32,
    name: &'a str
}

#[allow(unused_assignments)]
pub fn get_common_names(p_plant: &Plant) -> Vec<CommonName> {
    let pool = ConPool::get_connection();
    let mut common_n: Vec<CommonName> = vec![];
    common_n = CommonName::belonging_to(p_plant).load(&*pool.get().unwrap()).unwrap_or_default();
    common_n
}

pub fn create_common_name<'a>(p_plant_id: i32, p_name: &'a str) -> CommonName {
    let pool = ConPool::get_connection();
    let new_common_name = NewCommonName {
        plant_id: p_plant_id,
        name: p_name,
    };
    diesel::insert(&new_common_name).into(common_names::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new post")
}

#[allow(unused_must_use)]
pub fn update_common_name(p_common_name: &CommonName) {
    use self::common_names::dsl::*;
    let pool = ConPool::get_connection();
    let p_db_current = common_names.filter(id.eq(p_common_name.id)).get_result::<CommonName>(&*pool.get().unwrap()).unwrap();
    let mut update: bool = false;

    if p_common_name.name != p_db_current.name  {
        update = true;
    }

    if update {
        diesel::update(common_names.filter(id.eq(p_common_name.id)))
        .set(name.eq(&p_common_name.name))
        .execute(&*pool.get().unwrap());
    }
}

#[allow(unused_must_use)]
pub fn delete_common_name(cname_id: i32) {
    use self::common_names::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(common_names.filter(id.eq(cname_id)))
    .execute(&*pool.get().unwrap());
}