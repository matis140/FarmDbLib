use diesel;
use diesel::prelude::*;
use ConPool;

use chrono::{ NaiveDateTime };
use accounting_models::schema::accounting::initial_records;

/*
CREATE TABLE IF NOT EXISTS accounting.initial_records (
    id SERIAL NOT NULL PRIMARY KEY,
    "date" BIGINT NOT NULL,
    debit_gl_account_id INT REFERENCES accounting.chart_of_accounts(id),
    credit_gl_account_id INT REFERENCES accounting.chart_of_accounts(id),
    amount BIGINT NOT NULL
);
*/

#[derive(Insertable)]
#[table_name="initial_records"]
pub struct NewInitialRecord {
    pub date_time: NaiveDateTime,
    pub debit_gl_account_id: i32,
    pub credit_gl_account_id: i32,
    pub amount: i64,
}


#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(JournalRecord)]
pub struct InitialRecord {
    id: i32,
    date_time: NaiveDateTime,
    debit_gl_account_id: i32,
    credit_gl_account_id: i32,
    amount: i64,
}

impl InitialRecord {
    pub fn new(p_id: i32, p_date_time: NaiveDateTime, p_debit_gl_acct: i32, p_credit_gl_acct: i32, p_amount: i64) -> InitialRecord {
        InitialRecord {
            id: p_id,
            date_time: p_date_time,
            debit_gl_account_id: p_debit_gl_acct,
            credit_gl_account_id: p_credit_gl_acct,
            amount: p_amount,
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_date_time(&self) -> NaiveDateTime {
        self.date_time
    }

    pub fn get_debit_gl_account_id(&self) -> i32 {
        self.debit_gl_account_id
    }

    pub fn get_credit_gl_account_id(&self) -> i32 {
        self.credit_gl_account_id
    }

    pub fn get_amount(&self) -> i64 {
        self.amount
    }
}

#[allow(unused_assignments)]
pub fn get_initial_records() -> Option<Vec<InitialRecord>> {
    use self::initial_records::dsl::*;
    let pool = ConPool::get_connection();
    let d_initial_records = match initial_records.load::<InitialRecord>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    d_initial_records
}

pub fn get_initial_record(p_id: i32) -> Option<InitialRecord> {
    use self::initial_records::dsl::*;
    let pool = ConPool::get_connection();
    let dbrecord = match initial_records.filter(id.eq(p_id)).get_result::<InitialRecord>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dbrecord
}

pub fn create_initial_record(p_initial_record: &NewInitialRecord) -> InitialRecord {
    let pool = ConPool::get_connection();

    diesel::insert(p_initial_record)
        .into(initial_records::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new currency type")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_initial_record(p_initial_record: &InitialRecord) {
    use self::initial_records::dsl::*;
    let pool = ConPool::get_connection();

    diesel::update(initial_records.filter(id.eq(p_initial_record.id)))
        .set(p_initial_record)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_initial_record(p_initial_record_id: i32) {
    use self::initial_records::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(initial_records.filter(id.eq(p_initial_record_id)))
        .execute(&*pool.get().unwrap());
}