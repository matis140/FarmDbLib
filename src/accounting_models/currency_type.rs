use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::schema::accounting::currency_types;
//accounting
/*
CREATE TABLE IF NOT EXISTS accounting.currency_types (
    id SERIAL PRIMARY KEY NOT NULL,
    code VARCHAR NOT NULL,
    description VARCHAR NOT NULL
);
*/

#[derive(Insertable)]
#[table_name="currency_types"]
pub struct NewCategoryType {
    pub code: String,
    pub description: String,
}


#[allow(dead_code)]
#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(GeneralLedgerRecord)]
pub struct CurrencyType {
    pub id: i32,
    pub code: String,
    pub description: String,
}

#[allow(unused_assignments)]
pub fn get_category_types() -> Option<Vec<CurrencyType>> {
    use self::currency_types::dsl::*;
    let pool = ConPool::get_connection();
    let d_currency_type = match currency_types.load::<CurrencyType>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    d_currency_type
}

pub fn create_currency_type(p_currency_type: &NewCategoryType) -> CurrencyType {
    let pool = ConPool::get_connection();

    diesel::insert(p_currency_type)
        .into(currency_types::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new currency type")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_currency_type(p_currency_type: &CurrencyType) {
    use self::currency_types::dsl::*;
    let pool = ConPool::get_connection();
    
    diesel::update(currency_types.filter(id.eq(p_currency_type.id)))
        .set(p_currency_type)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_currency_type(p_currency_type: i32) {
    use self::currency_types::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(currency_types.filter(id.eq(p_currency_type)))
        .execute(&*pool.get().unwrap());
}
