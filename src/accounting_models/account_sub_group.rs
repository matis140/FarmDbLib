use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::account_group::AccountGroup;

use accounting_models::schema::accounting::account_sub_groups;

/*

CREATE TABLE IF NOT EXISTS accounting.account_sub_groups (
    id SERIAL PRIMARY KEY NOT NULL,
    account_group_id INT NOT NULL REFERENCES accounting.account_groups(id),
    name VARCHAR NOT NULL,
    dr_cr_char CHAR NOT NULL,
    status INT NOT NULL DEFAULT (0)
);
*/

#[derive(Insertable)]
#[table_name="account_sub_groups"]
pub struct NewAccountSubGroup {
    pub account_group_id: i32,
    pub name: String,
    pub dr_cr_char: String,
    pub status: i32,
}

#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(AccountGroup, ChartOfAccount)]
pub struct AccountSubGroup {
    id: i32,
    account_group_id: i32,
    name: String,
    dr_cr_char: String,
    status: i32,
}

impl AccountSubGroup {
    pub fn new(p_id: i32, p_account_group_id: i32, p_name: &str, p_dr_cr_char: &str, p_status: i32) -> AccountSubGroup {
        AccountSubGroup {
            id: p_id,
            account_group_id: p_account_group_id,
            name: p_name.to_string(),
            dr_cr_char: p_dr_cr_char.to_string(),
            status: p_status,
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_account_group_id(&self) -> i32 {
        self.account_group_id
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_dr_cr_char(&self) -> &str {
        &self.dr_cr_char
    }

    pub fn get_status(&self) -> i32 {
        self.status
    }

}

#[allow(unused_assignments)]
pub fn get_account_sub_groups() -> Option<Vec<AccountSubGroup>> {
    use self::account_sub_groups::dsl::*;
    let pool = ConPool::get_connection();
    let l_account_sub_groups = match account_sub_groups.load::<AccountSubGroup>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    l_account_sub_groups
}

#[allow(unused_assignments)]
pub fn get_account_sub_group(p_id: i32) -> Option<AccountSubGroup> {
    use self::account_sub_groups::dsl::*;
    let pool = ConPool::get_connection();
    let dbrecord = match account_sub_groups.filter(id.eq(p_id)).get_result::<AccountSubGroup>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dbrecord
}

pub fn create_account_sub_group(p_account_sub_group: &NewAccountSubGroup) -> AccountSubGroup {
    let pool = ConPool::get_connection();

    diesel::insert(p_account_sub_group)
        .into(account_sub_groups::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving account group")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_account_sub_group(p_account_sub_group: &AccountSubGroup) {
    use self::account_sub_groups::dsl::*;
    let pool = ConPool::get_connection();

    diesel::update(account_sub_groups.filter(id.eq(p_account_sub_group.id)))
        .set(p_account_sub_group)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_category_type(p_account_group_id: i32) {
    use self::account_sub_groups::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(account_sub_groups.filter(id.eq(p_account_group_id)))
        .execute(&*pool.get().unwrap());
}
