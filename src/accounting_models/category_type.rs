use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::schema::accounting::category_types;
//accounting
/*
CREATE TABLE IF NOT EXISTS accounting.category_type (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    debit_mod INT NOT NULL,
    credit_mod INT NOT NULL
);
*/

#[derive(Insertable)]
#[table_name="category_types"]
pub struct NewCategoryType<'a> {
    pub custom_id: i32,
    pub name: &'a str,
    pub debit_mod: i32,
    pub credit_mod: i32,
    pub status: i32
}


#[allow(dead_code)]
#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(AccountGroup, GeneralLedgerRecord)]
pub struct CategoryType {
    id: i32,
    custom_id: i32,
    name: String,
    debit_mod: i32,
    credit_mod: i32,
    status: i32,
}

impl CategoryType {
    pub fn new(p_id: i32, p_custom_id: i32, p_name: &str, p_debit_mod: i32, p_credit_mod: i32, p_status: i32) -> CategoryType {
        CategoryType {
            id: p_id,
            custom_id: p_custom_id,
            name: p_name.to_string(),
            debit_mod: p_debit_mod,
            credit_mod: p_credit_mod,
            status: p_status,
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_custom_id(&self) -> i32 {
        self.custom_id
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_debit_mod(&self) -> i32 {
        self.debit_mod
    }

    pub fn get_credit_mod(&self) -> i32 {
        self.credit_mod
    }

    pub fn get_status(&self) -> i32 {
        self.status
    }

    pub fn set_status(&mut self, p_status: i32) {
        self.status = p_status;
    }
}

pub fn get_category_type(p_id: i32) -> Option<CategoryType> {
    use self::category_types::dsl::*;
    let pool = ConPool::get_connection();
    let dbrecord = match category_types.filter(id.eq(p_id)).get_result::<CategoryType>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dbrecord
}

#[allow(unused_assignments)]
pub fn get_category_types() -> Option<Vec<CategoryType>> {
    use self::category_types::dsl::*;
    let pool = ConPool::get_connection();
    let dcategories = match category_types.load::<CategoryType>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dcategories
}

pub fn create_category_type<'a>(p_category_type: &'a NewCategoryType) -> CategoryType {
    let pool = ConPool::get_connection();

    diesel::insert(p_category_type)
        .into(category_types::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new category type")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_category_type(p_category_type: &CategoryType) {
    use self::category_types::dsl::*;
    let pool = ConPool::get_connection();
    let p_db_current = category_types.filter(id.eq(p_category_type.id)).get_result::<CategoryType>(&*pool.get().unwrap()).unwrap();
    let mut update: bool = false;

    update =
        p_category_type.name != p_db_current.name  ||
        p_category_type.credit_mod != p_db_current.credit_mod ||
        p_category_type.debit_mod != p_db_current.debit_mod ||
        p_category_type.custom_id != p_db_current.custom_id ||
        p_category_type.status != p_category_type.status;

    if update == true {
        diesel::update(category_types.filter(id.eq(p_category_type.id)))
            .set(p_category_type)
            .execute(&*pool.get().unwrap());
    }
}

#[allow(unused_must_use)]
pub fn delete_category_type(category_type_id: i32) {
    use self::category_types::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(category_types.filter(id.eq(category_type_id)))
        .execute(&*pool.get().unwrap());
}
