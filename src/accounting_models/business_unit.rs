use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::schema::accounting::business_units;

/*
CREATE TABLE IF NOT EXISTS accounting.business_units (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL
);*/

#[derive(Insertable)]
#[table_name="business_units"]
pub struct NewBusinessUnit {
    pub name: String,
    pub description: String,
}

#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(GeneralLedgerRecord)]
pub struct BusinessUnit {
    id: i32,
    name: String,
    description: String,
}

impl BusinessUnit {
    pub fn new(p_id: i32, p_name: &str, p_description: &str) -> BusinessUnit {
        BusinessUnit {
            id: p_id,
            name: p_name.to_string(),
            description: p_description.to_string(),
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_description(&self) -> &str {
        &self.description
    }
}

#[allow(unused_assignments)]
pub fn get_business_units() -> Option<Vec<BusinessUnit>> {
    use self::business_units::dsl::*;
    let pool = ConPool::get_connection();
    let l_business_units = match business_units.load::<BusinessUnit>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    l_business_units
}

#[allow(unused_assignments)]
pub fn get_business_unit(p_id: i32) -> Option<BusinessUnit> {
    use self::business_units::dsl::*;
    let pool = ConPool::get_connection();
    let dbrecord = match business_units.filter(id.eq(p_id)).get_result::<BusinessUnit>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dbrecord
}

pub fn create_business_unit(p_business_unit: &NewBusinessUnit) -> BusinessUnit {
    let pool = ConPool::get_connection();

    diesel::insert(p_business_unit)
        .into(business_units::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving business unit")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_business_unit(p_business_unit: &BusinessUnit) {
    use self::business_units::dsl::*;
    let pool = ConPool::get_connection();

    diesel::update(business_units.filter(id.eq(p_business_unit.id)))
        .set(p_business_unit)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_business_unit(p_business_unit_id: i32) {
    use self::business_units::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(business_units.filter(id.eq(p_business_unit_id)))
        .execute(&*pool.get().unwrap());
}
