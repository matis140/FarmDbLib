use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::RecordStatus;
use accounting_models::schema::accounting::chart_of_accounts;

/*
CREATE TABLE IF NOT EXISTS accounting.chart_of_accounts (
    id SERIAL NOT NULL PRIMARY KEY,
    gl_account_number INT NOT NULL,
    gl_account_name VARCHAR NOT NULL,
    gl_account_description VARCHAR NOT NULL,
    account_categories_id INT NOT NULL REFERENCES accounting.account_groups(id),
    account_sub_categories_id INT NOT NULL REFERENCES accounting.account_sub_groups(id),
    business_units_id INT NOT NULL REFERENCES accounting.business_units(id),
    status INT NOT NULL DEFAULT(0)
);
*/
#[derive(Insertable)]
#[table_name="chart_of_accounts"]
pub struct NewChartOfAccount {
    gl_account_number: i32,
    gl_account_name: String,
    gl_account_description: String,
    account_categories_id: i32,
    account_sub_categories_id: i32,
    business_units_id: i32,
    status: i32,
}

impl NewChartOfAccount {
    pub fn new(p_gl_account_number: i32, p_gl_account_name: &str, p_gl_account_desc: &str, p_account_category_id: i32, p_account_sub_category_id: i32, p_business_unit_id: i32, p_status: i32)
        -> NewChartOfAccount {
        NewChartOfAccount {
            gl_account_number: p_gl_account_number,
            gl_account_name: p_gl_account_name.to_string(),
            gl_account_description: p_gl_account_desc.to_string(),
            account_categories_id: p_account_category_id,
            account_sub_categories_id: p_account_sub_category_id,
            business_units_id: p_business_unit_id,
            status: p_status,
        }
    }
}

#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(InitialRecord, GeneralLedgerRecord)]
pub struct ChartOfAccount {
    id: i32,
    gl_account_number: i32,
    gl_account_name: String,
    gl_account_description: String,
    account_categories_id: i32,
    account_sub_categories_id: i32,
    business_units_id: i32,
    status: i32,
}

impl ChartOfAccount {
    pub fn new(p_id: i32, p_gl_act_number: i32, p_gl_act_name: &str, p_act_desc: &str, p_act_cat_id: i32, p_act_sub_cat_id: i32, p_business_unit_id: i32, p_status: i32)
        -> ChartOfAccount {
        ChartOfAccount {
            id: p_id,
            gl_account_number: p_gl_act_number,
            gl_account_name: p_gl_act_name.to_string(),
            gl_account_description: p_act_desc.to_string(),
            account_categories_id: p_act_cat_id,
            account_sub_categories_id: p_act_sub_cat_id,
            business_units_id: p_business_unit_id,
            status: p_status,
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn set_gl_account_number(&mut self, p_gl_number: i32) {
        self.gl_account_number = p_gl_number;
    }

    pub fn get_gl_account_number(&self) -> i32 {
        self.gl_account_number
    }

    pub fn set_gl_account_name(&mut self, p_gl_name: &str) {
        self.gl_account_name = p_gl_name.to_string();
    }

    pub fn get_gl_account_name(&self) -> &str {
        &self.gl_account_name
    }

    pub fn set_gl_account_description(&mut self, p_description: &str) {
        self.gl_account_description = p_description.to_string();
    }

    pub fn get_gl_account_description(&self) -> &str {
        &self.gl_account_description
    }

    pub fn set_account_categories_id(&mut self, p_account_categories_id: i32) {
        self.account_categories_id = p_account_categories_id;
    }

    pub fn get_account_categories_id(&self) -> i32 {
        self.account_categories_id
    }

    pub fn set_account_sub_categories_id(&mut self, p_account_sub_categories_id: i32) {
        self.account_sub_categories_id = p_account_sub_categories_id;
    }

    pub fn get_account_sub_categories_id(&self) -> i32 {
        self.account_sub_categories_id
    }

    pub fn set_business_unit_id(&mut self, p_business_unit_id: i32) {
        self.business_units_id = p_business_unit_id;
    }

    pub fn get_business_unit_id(&self) -> i32 {
        self.business_units_id
    }

    pub fn set_status(&mut self, p_status: RecordStatus) {
        self.status = p_status as i32;
    }

    pub fn get_status(&self) -> RecordStatus {
        match self.status {
            0 => RecordStatus::Active,
            1 => RecordStatus::Archived,
            _ => RecordStatus::NotSet
        }
    }

    pub fn get_status_i32(&self) -> i32 {
        self.status
    }
}

#[allow(unused_assignments)]
pub fn get_chart_of_accounts() -> Option<Vec<ChartOfAccount>> {
    use self::chart_of_accounts::dsl::*;
    let pool = ConPool::get_connection();
    let l_accounts = match chart_of_accounts.load::<ChartOfAccount>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    l_accounts
}

pub fn get_chart_of_account(p_id: i32) -> Option<ChartOfAccount> {
    use self::chart_of_accounts::dsl::*;
    let pool = ConPool::get_connection();
    let dbrecord = match chart_of_accounts.filter(id.eq(p_id)).get_result::<ChartOfAccount>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dbrecord
}

pub fn create_account(p_account: &NewChartOfAccount) -> ChartOfAccount {
    let pool = ConPool::get_connection();

    diesel::insert(p_account)
        .into(chart_of_accounts::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving chart of accounts")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_chart_of_account(p_account: &ChartOfAccount) {
    use self::chart_of_accounts::dsl::*;
    let pool = ConPool::get_connection();

    diesel::update(chart_of_accounts.filter(id.eq(p_account.id)))
        .set(p_account)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_chart_of_account(p_account_id: i32) {
    use self::chart_of_accounts::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(chart_of_accounts.filter(id.eq(p_account_id)))
        .execute(&*pool.get().unwrap());
}

