use diesel;
use diesel::prelude::*;
use ConPool;

use farm_models::schema::farm::products;

#[derive(Queryable, Identifiable, PartialEq)]
pub struct Product {
    pub id: i32,
    pub name: String,
    pub description: String,
}

#[derive(Insertable)]
#[table_name="products"]
pub struct NewProduct<'a> {
    pub name: &'a str,
    pub description: &'a str,
}

pub fn create_product<'a>(p_name: &'a str, p_description: &'a str) -> Product {
    let new_product: NewProduct = NewProduct {
        name: p_name,
        description: p_description,
    };
    let pool = ConPool::get_connection();
    diesel::insert(&new_product)
        .into(products::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new product")
}

pub fn get_product(p_id: i32) -> Option<Product> {
    use self::products::dsl::*;
    let pool = ConPool::get_connection();
    let dproduct = match products.filter(id.eq(p_id)).get_result::<Product>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dproduct
}

pub fn get_products() -> Option<Vec<Product>> {
    use self::products::dsl::*;
    let pool = ConPool::get_connection();
    let dproducts = match products.load::<Product>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dproducts
}

pub fn update_product(p_product: Product) -> bool {
    use self::products::dsl::*;
    let pool = ConPool::get_connection();
    let p_db_current = products.filter(id.eq(p_product.id)).get_result::<Product>(&*pool.get().unwrap()).unwrap();
    let mut update: bool = false;

    if p_product.name != p_db_current.name ||
        p_product.description != p_db_current.description{
        update = true;
    }

    let mut did_update: bool = false;
    if update {
        did_update = update_dif_columns(&p_db_current, &p_product);
    }
    if update && !did_update {
        update = false;
    }

    update
}

#[allow(unused_must_use)]
fn update_dif_columns(p_orig: &Product, p_targ: &Product) -> bool {
    let mut complete: bool = false;
    use self::products::dsl::*;
    let pool = ConPool::get_connection();
    if !p_orig.name.eq(&p_targ.name) {
        diesel::update(products.filter(id.eq(p_targ.id)))
            .set(name.eq(&p_targ.name))
            .execute(&*pool.get().unwrap());
        complete = true;
    }
    if !p_orig.description.eq(&p_targ.description) {
        diesel::update(products.filter(id.eq(p_targ.id)))
            .set(description.eq(&p_targ.description))
            .execute(&*pool.get().unwrap());
        complete = true;
    }

    complete
}
