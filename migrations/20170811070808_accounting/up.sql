-- Your SQL goes here
CREATE SCHEMA accounting;

CREATE TABLE IF NOT EXISTS accounting.category_types (
    id SERIAL PRIMARY KEY NOT NULL,
    custom_id INT NOT NULL,
    name VARCHAR NOT NULL,
    debit_mod INT NOT NULL,
    credit_mod INT NOT NULL,
    status INT NOT NULL DEFAULT(0)
);

INSERT INTO accounting.category_types (custom_id, name, debit_mod, credit_mod) VALUES (1000, 'Asset', 1, -1);
INSERT INTO accounting.category_types (custom_id, name, debit_mod, credit_mod) VALUES (2000, 'Liability', -1, 1);
INSERT INTO accounting.category_types (custom_id, name, debit_mod, credit_mod) VALUES (3000, 'Revenue', -1, 1);
INSERT INTO accounting.category_types (custom_id, name, debit_mod, credit_mod) VALUES (4000, 'Expense', 1, -1);
INSERT INTO accounting.category_types (custom_id, name, debit_mod, credit_mod) VALUES (5000, 'Capital', -1, 1);

CREATE TABLE IF NOT EXISTS accounting.account_groups (
    id SERIAL PRIMARY KEY NOT NULL,
    category_type_id INT NOT NULL REFERENCES accounting.category_types(id),
    name VARCHAR NOT NULL,
    dr_cr_char CHAR NOT NULL,
    status INT NOT NULL DEFAULT(0)
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 from information_schema.tables WHERE  table_schema = 'public' AND table_name = 'tmp_account_groups') THEN
        INSERT INTO accounting.account_groups
            SELECT * FROM public.tmp_account_groups ORDER BY id ASC;

        PERFORM setval('accounting.account_groups_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.account_groups), 1), false);

        DROP TABLE public.tmp_account_groups;
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS accounting.account_sub_groups (
    id SERIAL PRIMARY KEY NOT NULL,
    account_group_id INT NOT NULL REFERENCES accounting.account_groups(id),
    name VARCHAR NOT NULL,
    dr_cr_char CHAR NOT NULL,
    status INT NOT NULL DEFAULT (0)
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 from information_schema.tables WHERE  table_schema = 'public' AND table_name = 'tmp_account_sub_groups') THEN
        INSERT INTO accounting.account_sub_groups
            SELECT * FROM public.tmp_account_sub_groups ORDER BY id ASC;

        PERFORM setval('accounting.account_sub_groups_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.account_sub_groups), 1), false);

        DROP TABLE public.tmp_account_sub_groups;
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS accounting.business_units (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 from information_schema.tables WHERE table_schema = 'public' AND table_name = 'tmp_business_units') THEN
        INSERT INTO accounting.business_units
            SELECT * FROM public.tmp_business_units ORDER BY id ASC;

        PERFORM setval('accounting.business_units_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.business_units), 1), false);

        DROP TABLE public.tmp_business_units;
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS accounting.currency_types (
    id SERIAL PRIMARY KEY NOT NULL,
    code VARCHAR NOT NULL,
    description VARCHAR NOT NULL
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 from information_schema.tables WHERE table_schema = 'public' AND table_name = 'tmp_currency_types') THEN
        INSERT INTO accounting.currency_types
            SELECT * FROM public.tmp_currency_types ORDER BY id ASC;

        PERFORM setval('accounting.currency_types_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.currency_types), 1), false);

        DROP TABLE public.tmp_currency_types;
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS accounting.chart_of_accounts (
    id SERIAL NOT NULL PRIMARY KEY,
    gl_account_number INT NOT NULL,
    gl_account_name VARCHAR NOT NULL,
    gl_account_description VARCHAR NOT NULL,
    account_categories_id INT NOT NULL REFERENCES accounting.account_groups(id),
    account_sub_categories_id INT NOT NULL REFERENCES accounting.account_sub_groups(id),
    business_units_id INT NOT NULL REFERENCES accounting.business_units(id),
    status INT NOT NULL DEFAULT(0)
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'tmp_chart_of_accounts') THEN
        INSERT INTO accounting.chart_of_accounts
            SELECT * FROM public.tmp_chart_of_accounts ORDER BY id ASC;

        PERFORM setval('accounting.chart_of_accounts_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.chart_of_accounts), 1), false);

        DROP TABLE public.tmp_chart_of_accounts;
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS accounting.initial_records (
    id SERIAL NOT NULL PRIMARY KEY,
    date_time TIMESTAMP NOT NULL DEFAULT(now()),
    debit_gl_account_id INT NOT NULL REFERENCES accounting.chart_of_accounts(id),
    credit_gl_account_id INT NOT NULL REFERENCES accounting.chart_of_accounts(id),
    amount BIGINT NOT NULL
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'tmp_initial_records') THEN
        INSERT INTO accounting.initial_records
            SELECT * FROM public.tmp_initial_records ORDER BY "date_time" ASC;

        PERFORM setval('accounting.initial_records_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.initial_records), 1), false);

        DROP TABLE public.tmp_initial_records;
    END IF;
END $$;