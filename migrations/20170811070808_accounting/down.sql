-- This file should undo anything in `up.sql`

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'initial_records') THEN
        CREATE TABLE public.tmp_initial_records AS (SELECT * FROM accounting.initial_records);
    END IF;
END $$;

DROP TABLE accounting.initial_records;

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'chart_of_accounts') THEN
        CREATE TABLE public.tmp_chart_of_accounts AS (SELECT * FROM accounting.chart_of_accounts);
    END IF;
END $$;

DROP TABLE accounting.chart_of_accounts;

DO $$
BEGIN
    IF EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'business_units') THEN
        CREATE TABLE public.tmp_business_units AS (SELECT * FROM accounting.business_units);
    END IF;
END $$;

DROP TABLE accounting.business_units;

DO $$
BEGIN
    IF EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'account_sub_groups') THEN
        CREATE TABLE public.tmp_account_sub_groups AS (SELECT * FROM accounting.account_sub_groups);
    END IF;
END $$;

DROP TABLE accounting.account_sub_groups;

DO $$
BEGIN
    IF EXISTS(SELECT 1 from information_schema.tables WHERE  table_schema = 'accounting' AND table_name = 'account_groups') THEN
        CREATE TABLE public.tmp_account_groups AS (SELECT * FROM accounting.account_groups);
    END IF;
END $$;

DROP TABLE accounting.account_groups;

DO $$
BEGIN
    IF EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'currency_types') THEN
        CREATE TABLE public.tmp_currency_types AS (SELECT * FROM accounting.currency_types);
    END IF;
END $$;

DROP TABLE accounting.category_types;
DROP TABLE accounting.currency_types;

DROP SCHEMA accounting CASCADE;

