-- Your SQL goes here
CREATE SCHEMA IF NOT EXISTS farm;

CREATE TABLE farm.products (
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR NOT NULL,
  description VARCHAR NOT NULL
);

DO $$
BEGIN
  IF EXISTS(SELECT 1 from information_schema.tables WHERE  table_schema = 'public' AND table_name = 'tmp_products') THEN
    INSERT INTO farm.products(name, description)
      SELECT name, description FROM public.tmp_products ORDER BY id ASC;

    DROP TABLE public.tmp_products;
  END IF;
END $$;