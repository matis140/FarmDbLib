-- This file should undo anything in `up.sql`
DO $$
BEGIN
  IF EXISTS(SELECT 1 from information_schema.tables WHERE  table_schema = 'farm' AND table_name = 'products') THEN
    CREATE TABLE public.tmp_products AS (SELECT * FROM farm.products);
  END IF;
END $$;

DROP TABLE farm.products;

DROP SCHEMA farm CASCADE;