-- This file should undo anything in `up.sql`

DO $$
BEGIN
    IF EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'general_ledger_records') THEN
        CREATE TABLE public.tmp_general_ledger_records AS (SELECT * FROM accounting.general_ledger_records);
    END IF;
END $$;

DROP TABLE accounting.general_ledger_records;

DO $$
BEGIN
    IF EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = 'accounting' AND table_name = 'journal_records') THEN
        CREATE TABLE public.tmp_journal_records AS (SELECT * FROM accounting.journal_records);
    END IF;
END $$;

DROP TABLE accounting.journal_records;

